# build-docker-image.yml

Builds a Docker Image using Kaniko. Shamelessly cribbed from [this Gitlab example](https://gitlab.com/guided-explorations/containers/kaniko-docker-build).

## Usage

1) Include the template in your `.gitlab-ci.yml` file:

```yml
 include:
  - project: 'yarthur/pipeline-library'
    ref: 'main'
    file: 'jobs/build-docker-image.yml'
```

2) Create a job that extends this one:

```yml
build:
    extends: ".build-docker-image"
```

### Values

There are two custom values used in this job:

- `VERSIONLABELMETHOD`: [**"OnlyIfThisCommitHasVersion"** | "LastVersionTagInGit"]

    `OnlyIfThisCommitHasVersion` will set a version label only if the current commit this job is running on has a git tag set.

    `LastVersionTagInGit` sets the version label to the last tag set in the repo.

    Any other value will leave `VERSIONLABEL` alone.

- `VERSIONLABEL`

    If you want to set a custom version label, you can set this value directly. If that's the case, make sure to set `VERSIONLABELMETHOD` to some value other than the two defined, else you run the risk of the tag functionality overwriting your value.

The following [pre-defined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) are also used:

- `CI_COMMIT_BRANCH`
- `CI_COMMIT_REF_NAME`
- `CI_COMMIT_REF_SLUG`
- `CI_COMMIT_SHA`
- `CI_COMMIT_SHORT_SHA`
- `CI_COMMIT_TAG`
- `CI_DEFAULT_BRANCH`
- `CI_JOB_URL`
- `CI_MERGE_REQUEST_ID`
- `CI_PIPELINE_URL`
- `CI_PROJECT_DIR`
- `CI_PROJECT_TITLE`
- `CI_PROJECT_URL`
- `CI_REGISTRY`
- `CI_REGISTRY_IMAGE`
- `CI_REGISTRY_PASSWORD`
- `CI_REGISTRY_USER`
- `CI_SERVER_URL`
- `GITLAB_USER_EMAIL`
- `GITLAB_USER_LOGIN`

 All can be overwritten directly, or through setting environment variables in the repository settings.

```yml
build:
    extends: ".build-docker-image"
    # set variables directly
    variables:
        VERSIONLABELMETHOD: "LastVersionTagInGit"
    # Or set environment variables in the repo setting:
    environment:
        # Overrides CI_REGISTRY, CI_REGISTRY_IMAGE, CI_REGISTRY_USER and CI_REGISTRY_PASSWORD
        # to push to hub.docker.com
        name: push-to-docker-hub
```
