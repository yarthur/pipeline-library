## pipeline-library

Library of CI/CD pipeline processes and tools for any/all of my projects.

## Install

Add the following to the top of your `.gitlab-ci.yml` file:

```yml
include:
    - project: "yarthur/pipeline-library"
      ref: "main" # specifying a version is probably a better idea
      file: "<job>.yml"
```

## Maintainer

[John Arthur](https://gitlab.com/yarthur)

## Contribute

This is a personal library, and I don't intend to branch out to contributed
content.

That said, please feel free to
[log issues](https://gitlab.com/yarthur/pipeline-libary/issues) you may find,
such as typos, factual inaccuracies, or performance concerns. I make no
guarantee that I'll address them in a timely manner, but I promise I'll
appreciate the effort.

## License

[ISC](LICENSE) © John Arthur
